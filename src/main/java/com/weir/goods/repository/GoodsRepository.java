package com.weir.goods.repository;

import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;

import org.hibernate.query.sql.internal.NativeQueryImpl;
//import org.hibernate.query.internal.NativeQueryImpl;
import org.hibernate.transform.Transformers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weir.goods.entity.GoodsInfo;
import com.weir.quarkus.commons.vo.goods.GoodsInfoVo;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class GoodsRepository implements PanacheRepository<GoodsInfo> {

	@Inject
	EntityManager em;
	
	private static ObjectMapper objectMapper = new ObjectMapper();

	public List<GoodsInfoVo> getGoodsInfoByCart(Long memberId) {
		// 原生sql
		List resultList = em
				.createNativeQuery("select g.id,g.goods_name goodsName,g.price,c.quantity "
						+ "from goods_cart c join goods_info g on c.goods_id=g.id where c.member_id=?1")
				.setParameter(1, memberId).unwrap(NativeQueryImpl.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP) //转list<map>
				.getResultList();
		// list<map> 转 list<entity>
		List<GoodsInfoVo> readValue = null;
		try {
			readValue = objectMapper.readValue(objectMapper.writeValueAsString(resultList),
					new TypeReference<List<GoodsInfoVo>>() {
					});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return readValue;
	}

}
