package com.weir.goods.vo;

public class GoodsInfoVo {

	public Integer id;
	public Integer count;
	public String name;
	public GoodsInfoVo() {
		super();
	}
	public GoodsInfoVo(Integer id, Integer count, String name) {
		super();
		this.id = id;
		this.count = count;
		this.name = name;
	}
}
