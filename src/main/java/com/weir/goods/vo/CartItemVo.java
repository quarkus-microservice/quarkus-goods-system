package com.weir.goods.vo;

import java.util.List;

public class CartItemVo {

	public List<Integer> goodsId;
	public List<Integer> goodsCount;
}
