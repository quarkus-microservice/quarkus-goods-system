package com.weir.goods.resource;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.weir.goods.repository.GoodsRepository;
import com.weir.quarkus.commons.vo.goods.GoodsInfoVo;

import io.vertx.core.json.JsonObject;

import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

@Tag(name = "购物车管理")
@Path("goods/cart")
@ApplicationScoped
public class GoodsCartResource {
	
	private static final Logger LOG = Logger.getLogger(GoodsCartResource.class);
	
	@Inject GoodsRepository goodsRepository;
	
	@Operation(summary = "获取会员购物车商品")
	@GET
	@Path("/list/{memberId}")
	public List<GoodsInfoVo> list(@PathParam("memberId") Long memberId) {
		// 查询该会员购物车选中的商品
		List<GoodsInfoVo> list = goodsRepository.getGoodsInfoByCart(memberId);
		LOG.infof("list >{%s}", new JsonObject().put("list",list).toString());
		return list;
	}

}
