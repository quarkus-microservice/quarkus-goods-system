package com.weir.goods.mapper;

import org.mapstruct.Mapper;

import com.weir.goods.entity.GoodsInfo;
import com.weir.quarkus.commons.vo.goods.GoodsInfoVo;

@Mapper(componentModel = "cdi")
public interface GoodsInfoMapper {

	GoodsInfoVo toVo(GoodsInfo goodsInfo);
}
