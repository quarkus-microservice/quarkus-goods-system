package com.weir.goods.entity;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

/**
 * 商品属性分类表
 * @author weir
 *
 */
@Entity
@Table(name = "goods_attribute_category", schema = "weir-goods")
@org.hibernate.annotations.Table(appliesTo = "goods_attribute_category",comment = "商品属性分类表")
public class GoodsAttributeCategory extends PanacheEntityBase implements Serializable {

	private static final long serialVersionUID = -7055152101662670613L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	
	public String name;
}
