package com.weir.goods.entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

/**
 * 商品属性值表
 * @author weir
 *
 */
@Entity
@Table(name = "goods_attribute_value", schema = "weir-goods")
@org.hibernate.annotations.Table(appliesTo = "goods_attribute_value",comment = "商品属性值表")
public class GoodsAttributeValue extends PanacheEntityBase implements Serializable {

	private static final long serialVersionUID = -7055152101662670613L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	@Column(name = "product_id",columnDefinition = "bigint COMMENT '商品ID'")
	public Long productId;
	@Column(name = "attribute_id",columnDefinition = "bigint COMMENT '商品属性ID'")
	public Long attributeId;
	@Column(columnDefinition = "varchar(100) COMMENT '手动添加规格或参数的值，参数单值，规格有多个时以逗号隔开'")
	public String value;
}
