package com.weir.goods.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

/**
 * 商品促销活动
 * @author weir
 *
 */
@Entity
@Table(name = "goods_sales_promotion", schema = "weir-goods")
public class GoodsSalesPromotion extends PanacheEntityBase implements Serializable {

	private static final long serialVersionUID = -7900023543132546454L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	
//	@Column(columnDefinition = "bigint COMMENT '商品ID'")
    public Long productId;
	
	@Column(name = "count", columnDefinition = "int COMMENT '满足的商品数量'")
	public Integer count;
	@Column(name = "discount", columnDefinition = "datetime(6) COMMENT '折扣'")
	public BigDecimal discount;
	@Column(name = "price", columnDefinition = "datetime(6) COMMENT '折后价格'")
	public BigDecimal price;
	
	@Column(name = "full_price", columnDefinition = "datetime(6) COMMENT '商品满足金额'")
	public BigDecimal fullPrice;
	@Column(name = "reduce_price", columnDefinition = "datetime(6) COMMENT '商品减少金额'")
	public BigDecimal reducePrice;
	
	@Column(name = "member_level_id", columnDefinition = "bigint COMMENT '会员等级ID'")
	public Long memberLevelId;
	@Column(name = "member_price", columnDefinition = "datetime(6) COMMENT '会员价格'")
	public BigDecimal memberPrice;
	@Column(name = "member_level_name", columnDefinition = "varchar(100) COMMENT '会员等级名称'")
	public String memberLevelName;
	
	
}
