package com.weir.goods.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * 购物车表
 * @author weir
 *
 */
@Entity
@Table(name = "goods_cart")
@org.hibernate.annotations.Table(appliesTo = "goods_cart",comment = "购物车表")
public class GoodsCart extends BaseEntity {
	
	private static final long serialVersionUID = -1910453830672202779L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	
	@Column(name = "goods_id")
	public Long goodsId;
	@Column(name = "member_id")
	public Long memberId;
	/**
	 * 价格
	 */
	public String price;
	/**
	 * 数量
	 */
	public Integer quantity;

}
