package com.weir.goods.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * 商品品牌
 * @author weir
 *
 */
@Entity
@Table(name = "goods_brand", schema = "weir-goods")
@org.hibernate.annotations.Table(appliesTo = "goods_brand",comment = "商品品牌")
public class GoodsBrand extends PanacheEntityBase implements Serializable {
	private static final long serialVersionUID = 4291850786054658017L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;

	public String name;

	public Integer sort;

	public Boolean enable;

    @Column(columnDefinition = "varchar(100) COMMENT '品牌logo'", length = 100)
    public String logo;
    @Column(name = "big_pic",columnDefinition = "varchar(100) COMMENT '专区大图'", length = 100)
    public String bigPic;
    
    public String remark;

}