package com.weir.goods.entity;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

/**
 * 商品评论及回复
 * @author weir
 *
 */
@Entity
@Table(name = "goods_comment", schema = "weir-goods")
public class GoodsComment extends PanacheEntityBase  implements Serializable {
	private static final long serialVersionUID = 5575317994038584366L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;

	@Column(name = "product_id", columnDefinition = "bigint COMMENT '商品ID'")
    public Long productId;
	@Column(name = "member_id", columnDefinition = "bigint COMMENT '会员ID'")
	public Long memberId;

	@Column(name = "member_nick_name", columnDefinition = "varchar(50) COMMENT '会员昵称'")
    public String memberNickName;

	@Column(name = "product_name", columnDefinition = "varchar(100) COMMENT '商品名称'")
    public String productName;

	@Column(name = "star", columnDefinition = "int COMMENT '评价星数：0->5'")
    public Integer star;

	@Column(name = "member_ip", columnDefinition = "varchar(100) COMMENT '评价的IP'")
    public String memberIp;
	@Column(name = "create_time")
    public Date createTime;

	@Column(name = "enable", columnDefinition = "bit(1) COMMENT '是否显示'")
    public Boolean enable;

	@Column(name = "product_attribute", columnDefinition = "varchar(100) COMMENT '购买时的商品属性'")
    public String productAttribute;

	@Column(name = "pics", columnDefinition = "varchar(500) COMMENT '上传图片地址，以逗号隔开'")
    public String pics;

	@Column(length = 500)
    public String content;
	@Column(name = "replay", columnDefinition = "bit(1) COMMENT '是否回复'")
	public Boolean replay;
	@Column(name = "replay_type", columnDefinition = "int COMMENT '评论人员类型；0->会员；1->管理员'")
	public Integer replayType;
}