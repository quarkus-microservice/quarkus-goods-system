package com.weir.goods.entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

/**
 * 商品属性表
 * @author weir
 *
 */
@Entity
@Table(name = "goods_attribute", schema = "weir-goods")
@org.hibernate.annotations.Table(appliesTo = "goods_attribute",comment = "商品属性表")
public class GoodsAttribute extends PanacheEntityBase implements Serializable {

	private static final long serialVersionUID = 332072739094921432L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	
	@ManyToOne
	@JoinColumn(name="attribute_category_id",columnDefinition = "bigint COMMENT '属性分类ID'")
	public GoodsAttributeCategory attributeCategoryId;
	
	public String name;
	
	@Column(columnDefinition = "int COMMENT '属性的类型；0->规格；1->参数'")
    public Integer type;
	
	@Column(name = "select_type",columnDefinition = "int COMMENT '属性选择类型：0->唯一；1->单选；2->多选'")
    public Integer selectType;

	@Column(name = "inout_type",columnDefinition = "int COMMENT '属性录入方式：0->手工录入；1->从列表中选取'")
    public Integer inputType;

	@Column(name = "input_list",columnDefinition = "varchar(100) COMMENT '可选值列表，以逗号隔开'")
    public String inputList;

	@Column(columnDefinition = "int COMMENT '排序字段：最高的可以单独上传图片'")
    public Integer sort;

	@Column(name = "filter_type",columnDefinition = "int COMMENT '分类筛选样式：1->普通；1->颜色'")
    public Integer filterType;

	@Column(name = "related_status",columnDefinition = "int COMMENT '相同属性产品是否关联；0->不关联；1->关联'")
    public Integer relatedStatus;

	@Column(name = "hand_add_status",columnDefinition = "int COMMENT '是否支持手动新增；0->不支持；1->支持'")
    public Integer handAddStatus;
}
