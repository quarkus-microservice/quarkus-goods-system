package com.weir.goods.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

/**
 * 商品SKU表(库存)
 * @author weir
 *
 */
@Entity
@Table(name = "goods_sku_stock")
public class GoodsSkuStock extends PanacheEntityBase implements Serializable {
	private static final long serialVersionUID = 3402509335882561483L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;

	@Column(name = "product_id", columnDefinition = "bigint COMMENT '商品ID'")
    public Long productId;

    @Column(name="sku_code",columnDefinition = "varchar(100) COMMENT 'sku编码'")
    public String skuCode;

    public BigDecimal price;

    @Column(name="stock",columnDefinition = "int COMMENT '库存'")
    public Integer stock;

    @Column(name="low_stock",columnDefinition = "int COMMENT '预警库存'")
    public Integer lowStock;

    @Column(name="pic",columnDefinition = "varchar(100) COMMENT '展示图片'")
    public String pic;

    @Column(name="sale",columnDefinition = "int COMMENT '销量'")
    public Integer sale;

    @Column(name="promotion_price",columnDefinition = "datetime(6) COMMENT '单品促销价格'")
    public BigDecimal promotionPrice;

    @Column(name="lock_stock",columnDefinition = "int COMMENT '锁定库存'")
    public Integer lockStock;

    @Column(name="sp_data",columnDefinition = "varchar(500) COMMENT '商品销售属性，json格式'")
    public String spData;
}