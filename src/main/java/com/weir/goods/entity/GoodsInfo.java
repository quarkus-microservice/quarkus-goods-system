package com.weir.goods.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

/**
 * 商品表
 * 
 * @author weir
 *
 */
@Entity
@Table(name = "goods_info")
// 没起作用
@org.hibernate.annotations.Table(appliesTo = "goods_info",comment = "商品表") 
public class GoodsInfo extends PanacheEntityBase implements Serializable {

	private static final long serialVersionUID = -1910453830672202779L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;

	@Column(name = "brand_id", columnDefinition = "bigint COMMENT '品牌ID'")
	private Long brandId;

	@Column(name = "category_id", columnDefinition = "bigint COMMENT '商品分类ID'")
	private Long categoryId;

	@Column(name = "feight_template_id", columnDefinition = "bigint COMMENT '运费模版ID'")
	private Long feightTemplateId;

	@Column(name = "attribute_category_id", columnDefinition = "bigint COMMENT '商品属性分类ID'")
	private Long attributeCategoryId;

	private String name;

	private String pic;

	@Column(name = "product_sn", columnDefinition = "varchar(50) COMMENT '货号'")
	private String productSn;

	@Column(columnDefinition = "bit(1) COMMENT '删除状态：0->未删除；1->已删除'")
	private Boolean enable;

	@Column(name = "publish_status", columnDefinition = "int COMMENT '上架状态：0->下架；1->上架'")
	private Integer publishStatus;

	@Column(name = "new_status", columnDefinition = "int COMMENT '新品状态:0->不是新品；1->新品'")
	private Integer newStatus;

	@Column(name = "recommand_status", columnDefinition = "int COMMENT '推荐状态；0->不推荐；1->推荐'")
	private Integer recommandStatus;

	@Column(name = "verify_status", columnDefinition = "int COMMENT '审核状态：0->未审核；1->审核通过'")
	private Integer verifyStatus;

	@Column(columnDefinition = "int COMMENT '排序'")
	private Integer sort;

	@Column(columnDefinition = "int COMMENT '销量'")
	private Integer sale;

	private BigDecimal price;

	@Column(name = "promotion_price", columnDefinition = "datetime(6) COMMENT '促销价格'")
	private BigDecimal promotionPrice;

	@Column(name = "gift_growth", columnDefinition = "int COMMENT '赠送的成长值'")
	private Integer giftGrowth;

	@Column(name = "gift_point", columnDefinition = "int COMMENT '赠送的积分'")
	private Integer giftPoint;

	@Column(name = "use_point_limit", columnDefinition = "int COMMENT '限制使用的积分数'")
	private Integer usePointLimit;

	@Column(name = "sub_title", columnDefinition = "varchar(100) COMMENT '副标题'")
	private String subTitle;

	@Column(name = "original_price", columnDefinition = "datetime(6) COMMENT '市场价'")
	private BigDecimal originalPrice;

	@Column(columnDefinition = "int COMMENT '库存'")
	private Integer stock;

	@Column(name = "low_stock", columnDefinition = "int COMMENT '库存预警值'")
	private Integer lowStock;

	@Column(columnDefinition = "varchar(20) COMMENT '单位'")
	private String unit;

	@Column(columnDefinition = "datetime(6) COMMENT '商品重量，默认为克'")
	private BigDecimal weight;

	@Column(name = "preview_status", columnDefinition = "int COMMENT '是否为预告商品：0->不是；1->是'")
	private Integer previewStatus;

	@Column(columnDefinition = "varchar(20) COMMENT '以逗号分割的产品服务：1->无忧退货；2->快速退款；3->免费包邮'")
	private String services;

	private String keywords;

	private String note;

	@Column(name = "album_pics", columnDefinition = "varchar(500) COMMENT '画册图片，连产品图片限制为5张，以逗号分割'")
	private String albumPics;

	@Column(name = "detail_title", columnDefinition = "varchar(100) COMMENT '详情标题'")
	private String detailTitle;
	/**
	 * 详情描述
	 */
	@Column(name = "detail_desc", columnDefinition = "TEXT COMMENT '详情描述'")
	private String detailDesc;

	@Column(name = "promotion_start_time", columnDefinition = "DATETIME COMMENT '促销开始时间'")
	private Date promotionStartTime;

	@Column(name = "promotion_end_time", columnDefinition = "DATETIME COMMENT '促销结束时间'")
	private Date promotionEndTime;

	@Column(name = "promotion_per_limit", columnDefinition = "int COMMENT '活动限购数量'")
	private Integer promotionPerLimit;
/**
 * 促销类型：0->没有促销使用原价;1->使用促销价；2->使用会员价；3->使用阶梯价格；4->使用满减价格；5->限时购
 */
	@Column(name = "promotion_type",columnDefinition = "int COMMENT '促销类型：0->没有促销使用原价;1->使用促销价；2->使用会员价；3->使用阶梯价格；4->使用满减价格；5->限时购'")
	private Integer promotionType;
/**
 * 品牌名称
 */
	@Column(name = "brand_name",columnDefinition = "varchar(50) COMMENT '品牌名称'")
	private String brandName;
/**
 * 商品分类名称
 */
	@Column(name = "category_name",columnDefinition = "varchar(50) COMMENT '商品分类名称'")
	private String productCategoryName;
	/**
	 * 商品描述
	 */
	@Column(columnDefinition = "TEXT COMMENT '商品描述'")
	private String description;
	/**
	 * 产品详情网页内容
	 */
	@Column(columnDefinition = "TEXT COMMENT '产品详情网页内容'", name = "detail_html")
	private String detailHtml;
	/**
	 * 移动端网页详情
	 */
	@Column(columnDefinition = "TEXT COMMENT '移动端网页详情'", name = "detail_mobile_html")
	private String detailMobileHtml;

}
